"use strict"

// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser //
// При виклику функція повинна запитати ім'я та прізвище //

function createNewUser() {
    let firstName = prompt("What is your first name?");
    let lastName = prompt("What is your last name?");
    let newUser = {
        _username: firstName,
        _surname: lastName,
        getLogin() {
            return this._username[0].toLowerCase() + this._surname.toLowerCase();
        },   
    };

    Object.defineProperty(newUser, "username", {
        get(){
            console.log(`The first name is ${this._username}!`);
        },
        set(newValue){
            console.log("You are changing the value of a first name!");
            this._username = newValue;
        },
    });
    Object.defineProperty(newUser, "surname", {
        get(){
            console.log(`The last name is ${this._surname}!`);
        },
        set(newValue){
            console.log("You are changing the value of a last name!");
            this._surname = newValue;
        },
    });
    return newUser;
}

// Викликаємо функцію createNewUser() та записуємо результат(тобто об'єкт newUser)
let user = createNewUser();
console.log(user.getLogin());

